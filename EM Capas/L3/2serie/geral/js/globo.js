function globo(_corGlobo,_tipoMesh){
	window.onload=function(){
		var difinfo=document.getElementById("tudo"),
		diful=difinfo.getElementsByTagName('ul'),
		quantidade= difinfo.getElementsByTagName('li').length;
		console.log(quantidade);
		
		var $container = $('#container');
		var canvas = document.getElementById("menu");
		var linkSelecionado;
		var renderer = new THREE.WebGLRenderer({
			antialias: true,
			alpha: true,
			canvas:canvas
		});
		renderer.autoClear = false;
		var camera = new THREE.PerspectiveCamera(40, 1, 0.1, 10000);
		var scene = new THREE.Scene();
		var Ico;
		scene.fog = new THREE.Fog(0xd4d4d4, 25, 100);
		scene.add(camera);
		var group = new THREE.Group();
		scene.add( group );
		var groupTextos = new THREE.Group();
		scene.add( groupTextos );
		var groupCirculo1 = new THREE.Group();
		scene.add( groupCirculo1 );
		var groupCirculo2 = new THREE.Group();
		scene.add( groupCirculo2 );
		var groupCirculo3 = new THREE.Group();
		scene.add( groupCirculo3 );

		var raycaster = new THREE.Raycaster();
		var mouse = new THREE.Vector2(1, 1);
		var intersects = [];
		var mouseVector = new THREE.Vector3();
		var direction = new THREE.Vector3();
		$container.append(renderer.domElement);

		var txtLoader = new THREE.TextureLoader();
		txtLoader.setCrossOrigin("");
		var textures = ["resources/image/lixo1.png"];
		for (var i = 0, l = quantidade; i < l; i++) {
			textures.push(difinfo.getElementsByTagName('li')[i].getElementsByTagName('a')[0].innerText);
			
			var t=difinfo.getElementsByTagName('li')[i].getElementsByTagName('a')[0].innerText;
			textures.push(t.substring(0,t.length-4)+"over.png");
			textures.push(t.substring(0,t.length-4)+"texto.png");

		}
		var materiaisClicados=[];
		var texturesBolinhas = [];
		var textureBolinha = txtLoader.load(textures[0]);

		var mesh = new THREE.IcosahedronGeometry(10, 1); 
		var mesh2 = new THREE.IcosahedronGeometry(10, 1); 
		var vertices = mesh.vertices;
		var positions = new Float32Array(vertices.length * 3);
		for (var i = 0, l = vertices.length; i < l; i++) {

			vertices[i].toArray(positions, i * 3);

			var texture = txtLoader.load(texturesBolinhas[Math.floor(Math.random() * texturesBolinhas.length)]);
			var spriteMaterial = new THREE.SpriteMaterial({map: texture,transparent: true,
				opacity: 1});

			var sprite = new THREE.Sprite(spriteMaterial);
			sprite.scale.setScalar(1);
			direction.copy(vertices[i]).normalize();
			sprite.position.copy(vertices[i]).addScaledVector(direction, 1);
			groupTextos.add(sprite);
		}

		var materialPoint = new THREE.PointsMaterial({
			size: 2,
			map:textureBolinha,
			blending: THREE.AdditiveBlending,
			depthTest: false, transparent: true
		});

		var object = new THREE.Object3D();
		var corGlobo;
		if(_corGlobo){
			corGlobo=_corGlobo;
		}else{
			corGlobo=0x3b8cc3;
		}
		var hexagono=new THREE.Mesh(mesh,new THREE.MeshPhongMaterial({color: corGlobo,emissive: corGlobo,wireframe: true,fog: 1}));
		var hexagono2=new THREE.Mesh(mesh2, new THREE.MeshPhongMaterial({
			color: 0x009ACD,
			specular: 0x009ACD,
			flatShading: true,
			transparent: true,
			opacity: 0.25
		} ));
		//object.add(points);
		scene.add(object);

		if(_tipoMesh=="esferico"){
			var materialFaixas = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0xffffff } );
			var textureLoader = new THREE.TextureLoader();

			var textureFaixas = textureLoader.load( 'resources/image/faixas2.png' );
			textureFaixas.flipY = false;

			var materialFaixas=new THREE.MeshPhongMaterial({
				map:textureFaixas,
				transparent: true,
				blending: THREE.AdditiveBlending,
				side: THREE.DoubleSide,
				depthWrite: false, 
				depthTest: false,
				emissive: 0xffff00,
				opacity: 1}
				);

			var loader = new THREE.GLTFLoader();
			loader.load( 'resources/image/faixas.gltf', function ( gltf ) {
				var model = gltf.scene;
				model.traverse ( ( o ) => {
					if ( o.isMesh ) {
						o.material = materialFaixas;
					}
				} );

				object.add( model );

			} );
		}
		camera.position.z = 40;

		var L2 = new THREE.PointLight();
		L2.position.z = 1900;
		L2.position.y = 1850;
		L2.position.x = 1000;
		scene.add(L2);
		camera.add(L2);

		
		var numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40];
		var random = shuffle(numbers);
		var planos=[];
		var sprites=[];
		var spritesText=[];
		var j=1;
		for (var i = 0, l = quantidade; i < l; i++) {

			console.log(i);
			var texture = txtLoader.load(textures[j+1]);
			var textureText = txtLoader.load(textures[j+2]);
			var textureClicado = txtLoader.load(textures[j]);
			materiaisClicados.push(textureClicado);
			var spriteMaterial = new THREE.SpriteMaterial({map: texture,transparent: true,
				opacity: 1});
			var spriteMaterialText = new THREE.SpriteMaterial({map: textureText,transparent: true,
				opacity: 1});
			var sprite = new THREE.Sprite(spriteMaterial);
			var spriteText = new THREE.Sprite(spriteMaterialText);
			sprite.scale.setScalar(4.5);
			spriteText.scale.setScalar(14);
			direction.copy(vertices[numbers[i]]).normalize();
			sprite.position.copy(vertices[numbers[i]]).addScaledVector(direction, 1);
			sprite.botao=true;
			sprite.passaBarra=true;
			sprite.qual=i;
			sprite.linkClique=difinfo.getElementsByTagName('li')[i].getElementsByTagName('a')[1].innerText;
			sprite.frustumCulled = false; 
			sprites[i]=sprite;
			spriteText.position.copy(vertices[numbers[i]]).addScaledVector(direction, 1);
			group.add(sprite);
			groupTextos.add(spriteText);
			spritesText.push(spriteText);
			j+=3;
		}
		var trackballControl = new THREE.TrackballControls(camera, renderer.domElement,group,camera,mouseVector,materiaisClicados,sprites);
		trackballControl.rotateSpeed = 1.0; 
		trackballControl.noZoom = true;
		trackballControl.noPan  = true;

		var selectedObject = null;
		function shuffle(o) {
			for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
				return o;
		};

		
		function update() {
			object.rotation.x += 0.001;
			object.rotation.y += 0.001;
			group.rotation.x += 0.001;
			group.rotation.y += 0.001;
			groupTextos.rotation.x += 0.001;
			groupTextos.rotation.y += 0.001;
			for ( var i = 0, l = planos.length; i < l; i ++ ) {
				planos[ i ].lookAt( camera.position );
			}
		}
		var clock = new THREE.Clock();
		function render() {
			var a=new THREE.Vector3(0,0,40);
			
			var i=0;
			for(i=0;i<spritesText.length;i++){
				var d = camera.position.distanceTo( spritesText[i].getWorldPosition( a ) );
				//spritesText[i].material.opacity=0.5-Math.abs(d/40);
				if(d<32){
					spritesText[i].material.opacity=1.0;
				}else{
					spritesText[i].material.opacity=0;
				}

			}

			requestAnimationFrame(render);
			var delta = clock.getDelta();
			trackballControl.update(delta);
			//controls.update();
			renderer.render( scene, camera );
			update();
		}
		render();



	}
}